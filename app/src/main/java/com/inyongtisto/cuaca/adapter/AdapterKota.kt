package com.inyongtisto.cuaca.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.inyongtisto.cuaca.R
import com.inyongtisto.cuaca.sqlite.Kota
import java.util.ArrayList

class AdapterKota(private var data: ArrayList<Kota>, private var listener: Listeners) :
    RecyclerView.Adapter<AdapterKota.Holder>() {
    private var b: Int = 0

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): Holder {
        val view =
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_kota, viewGroup, false)
        return Holder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: Holder, i: Int) {
        val a = data[i]
        holder.label.text = a.nama

        holder.layout.setOnClickListener {
            listener.onClicked(a)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var layout: LinearLayout = itemView.findViewById(R.id.div_layout)
        var label: TextView = itemView.findViewById(R.id.tv_nama)
    }

    interface Listeners{
        fun onClicked(a: Kota)
    }
}
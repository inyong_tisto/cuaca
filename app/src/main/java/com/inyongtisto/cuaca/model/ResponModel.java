package com.inyongtisto.cuaca.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ResponModel implements Serializable {
    public ArrayList<Weather> weather = new ArrayList<>();
    public ResponModel main;
    public ResponModel wind;
    public Float temp;
    public Float pressure;
    public Float humidity;
    public Float temp_min;
    public Float temp_max;
    public Float speed;
    public Float deg;
    public String name;
}

package com.inyongtisto.cuaca.helper

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Build
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.inyongtisto.cuaca.R
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat

object Helper {
    fun translate(s: String, imageCuaca: ImageView):String{
        var text = s
        when (s){
            "Haze" -> {
                text = "mendung"
                Picasso.with(context)
                    .load(R.drawable.logo_berawan)
                    .into(imageCuaca)
            }
            "Clouds" -> {
                text = "Berawan"
                Picasso.with(context)
                    .load(R.drawable.ic_berawan)
                    .into(imageCuaca)
            }
            "Rain" -> {
                text = "Hujan"
                Picasso.with(context)
                    .load(R.drawable.ic_hujan)
                    .into(imageCuaca)
            }
            "Clear" -> {
                text = "cerah"
                Picasso.with(context)
                    .load(R.drawable.ic_cerah)
                    .into(imageCuaca)
            }
        }
        return text
    }

    fun pullRefrash(view: SwipeRefreshLayout, listener: Listeners) {
        view.setOnRefreshListener {
            listener.onRefrash()
        }

        view.setColorSchemeResources(
            R.color.colorAccent,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light
        )
    }

    lateinit var context : Activity
    lateinit var imgBgcuaca: ImageView
    lateinit var divMain: RelativeLayout
    @SuppressLint("SimpleDateFormat")
    fun ubahTema(
        context: Activity,
        imgBgcuaca: ImageView,
        divMain: RelativeLayout
    ){
        this.context = context
        this.imgBgcuaca = imgBgcuaca
        this.divMain = divMain

        val dateNow = System.currentTimeMillis()
        val sJam = SimpleDateFormat("kk")
        val jam: String = sJam.format(dateNow)

        //Mode Siang
        if (jam.toInt() in 6..18)modeSiang()

        //Mode malam
        if (jam.toInt() <= 5)modeMalam()
        if (jam.toInt() in 19..24)modeMalam()
    }

    fun modeSiang(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            val window: Window = context.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.setStatusBarColor(context.resources.getColor(R.color.white))
        }
        divMain.setBackgroundResource(R.color.siang)
        Picasso.with(context)
            .load(R.drawable.bg_siang)
            .into(imgBgcuaca)
        blackStatusBar(context)
    }

    fun modeMalam(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            val window: Window = context.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = context.resources.getColor(R.color.malam)
        }
        divMain.setBackgroundResource(R.color.malam)
        Picasso.with(context)
            .load(R.drawable.bg_malam)
            .into(imgBgcuaca)
    }

    fun blackStatusBar(context: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val decor = context.window.decorView
            decor.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
    }

    interface Listeners {
        fun onRefrash()
    }
}
package com.inyongtisto.cuaca.sqlite

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE


@Dao
interface DaoHistory {

    @Insert(onConflict = REPLACE)
    fun insert(data: Kota)

    @Delete
    fun delete(data: Kota)

    @Update
    fun update(data: Kota): Int

    @Query("SELECT * from kota ORDER BY idTb DESC")
    fun getAll(): List<Kota>

    @Query("DELETE FROM kota")
    fun deleteAll(): Int

    @Query("SELECT * FROM kota WHERE nama = :nama LIMIT 1")
    fun getDataByNama(nama: String): Kota
}
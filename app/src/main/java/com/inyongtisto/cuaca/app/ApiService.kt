package com.inyongtisto.cuaca.app

import com.inyongtisto.cuaca.model.ResponModel
import retrofit2.Call
import retrofit2.http.*

interface ApiService {
    @GET("weather")
    fun getCuaca(
        @Query("appid") id: String,
        @Query("q") nama: String
    ): Call<ResponModel>

    @GET("weather")
    fun getCuaca(
        @Query("appid") id: String,
        @Query("lat") lat: String,
        @Query("lon") log: String
    ): Call<ResponModel>
}
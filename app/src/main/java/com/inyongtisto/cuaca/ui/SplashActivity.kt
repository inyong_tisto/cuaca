package com.inyongtisto.cuaca.ui

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.inyongtisto.cuaca.R
import com.inyongtisto.cuaca.helper.Helper
import com.inyongtisto.cuaca.ui.main.MainActivity
import com.inyongtisto.cuaca.util.GPSTracker
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity(){

    private val MY_PERMITION_REQUEST_CODE = 7192
    private lateinit var gps: GPSTracker

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Helper.ubahTema(this, img_bgCuaca, div_main)

        gps = GPSTracker(this)

        Handler().postDelayed({
            startActivity(Intent(applicationContext, MainActivity::class.java))
            finish()
        }, 3000)

        if (gps.canGetLocation()) {
            cekLokasi()
        } else {
            gps.showSettingsAlert()
        }
    }

    private fun cekLokasi() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {

            //  Jika permition Acces lokasi belum di berikan
            ActivityCompat.requestPermissions(this,
                arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ),
                MY_PERMITION_REQUEST_CODE
            )
        } else {
            // jika Permition sudah diberikan
            gps.location
            Log.d("Respons", " Get lokasi \nLatitude: "+ gps.getlatitude() +" \nLongitude: " + gps.getlongitude())
        }
    }
}

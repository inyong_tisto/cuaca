package com.inyongtisto.cuaca.ui.main

import com.inyongtisto.cuaca.app.ApiConfig
import com.inyongtisto.cuaca.app.ApiService
import com.inyongtisto.cuaca.model.ResponModel
import com.inyongtisto.cuaca.util.Config
import com.inyongtisto.cuaca.util.Corountines
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainPresenter :MainContract.Presenter {

    private lateinit var view: MainContract.View
    private var api : ApiService = ApiConfig.instanceRetrofit

    override fun getData(nama:String) {
        Corountines.main {
            api.getCuaca(Config.api, nama).enqueue(object : Callback<ResponModel> {
                override fun onResponse(call: Call<ResponModel>, response: Response<ResponModel>) {
                    if (response.isSuccessful){
                        view.onSuccess(response.body()!!)
                    } else {
                        view.onError("Kota Tidak Ditemukan")
                    }
                }

                override fun onFailure(call: Call<ResponModel>, t: Throwable) {
                    view.onError(t.message!!)
                }
            })
        }
    }

    override fun getCuacaByLoc(lat: String, log: String) {
        Corountines.main {
            api.getCuaca(Config.api, lat, log).enqueue(object : Callback<ResponModel> {
                override fun onResponse(call: Call<ResponModel>, response: Response<ResponModel>) {
                    view.onSuccess(response.body()!!)
                }

                override fun onFailure(call: Call<ResponModel>, t: Throwable) {
                    view.onError(t.message!!)
                }
            })
        }
    }


    override fun subscribe() {

    }

    override fun unsubscribe() {

    }

    override fun attach(view: MainContract.View) {
        this.view = view
    }


}
package com.inyongtisto.cuaca.ui.main

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.inyongtisto.cuaca.R
import com.inyongtisto.cuaca.adapter.AdapterKota
import com.inyongtisto.cuaca.di.ActivityModule
import com.inyongtisto.cuaca.di.DaggerActivityComponent
import com.inyongtisto.cuaca.helper.DataDami
import com.inyongtisto.cuaca.helper.Helper
import com.inyongtisto.cuaca.model.ResponModel
import com.inyongtisto.cuaca.sqlite.Kota
import com.inyongtisto.cuaca.sqlite.MyDatabase
import com.inyongtisto.cuaca.util.GPSTracker
import es.dmoral.toasty.Toasty
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainContract.View{

    @Inject
    lateinit var presenter: MainContract.Presenter

    private lateinit var db: MyDatabase
    private val compositeDisposable = CompositeDisposable()
    private val MY_PERMITION_REQUEST_CODE = 7192
    private lateinit var gps: GPSTracker

    private lateinit var sheetBehavior: BottomSheetBehavior<RelativeLayout>
    private var bottomView: RelativeLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        Helper.ubahTema(this, img_bgCuaca, div_main)
        injectDependency()
        db = MyDatabase.getInstance(this)!!
        presenter.attach(this)
        Helper.pullRefrash(swipeRefrash, object : Helper.Listeners{
            override fun onRefrash() {
                presenter.getCuacaByLoc(""+gps.getlatitude(), ""+gps.getlongitude())
            }
        })
//        presenter.getData("semarang")

        setupGPS()
        setupBottomSheet()
        mainButton()
        autoComplite()
        getKota(db.daoHistoryKota().getAll() as ArrayList)
    }

    private fun setupGPS() {
        gps = GPSTracker(this)
        if (gps.canGetLocation()) {
            cekLokasi()
            println("can get location")
        } else {
            gps.showSettingsAlert()
        }
    }

    private fun mainButton() {
        btn_search.setOnClickListener {
            bottom_sheet.visibility = View.VISIBLE
            expandCloseSheet()
            getKota(db.daoHistoryKota().getAll() as ArrayList)
        }

        btn_cari.setOnClickListener {
            if (edt_namaKota.text.toString().isEmpty()){
                edt_namaKota.error = "Tidak boleh kosong"
                edt_namaKota.requestFocus()
                return@setOnClickListener
            }
            presenter.getData("semarang")
            hidekeyboard()
            expandCloseSheet()

        }
    }

    private fun autoComplite() {
        val adapter: ArrayAdapter<String> = ArrayAdapter(
            this,
            android.R.layout.simple_list_item_1,
            DataDami.KOTA1
        )
        edt_namaKota.setAdapter(adapter)
        edt_namaKota.setOnItemClickListener { parent, view, position, id ->
            hidekeyboard()
            expandCloseSheet()
            edt_namaKota.setText("")
            edt_namaKota.setHint("Nama Kota")
            presenter.getData("" +parent.getItemAtPosition(position))
            val kota = db.daoHistoryKota().getDataByNama(""+parent.getItemAtPosition(position))
            if (kota == null){
                val data = Kota()
                data.nama = ""+parent.getItemAtPosition(position)
                insertHistori(data)
            }
        }
    }

    private fun cekLokasi() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {

            //  Jika permition Acces lokasi belum di berikan
            ActivityCompat.requestPermissions(this,
                arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ),
                MY_PERMITION_REQUEST_CODE
            )
        } else {

            // jika Permition sudah diberikan
            gps.location
            Log.d("Respons", " Get lokasi \nLatitude: "+ gps.getlatitude() +" \nLongitude: " + gps.getlongitude())

            // get cuaca berdasarkan lokasi
            presenter.getCuacaByLoc(""+gps.getlatitude(), ""+gps.getlongitude())
            // presenter.getCuacaByLoc("-7.12", ""+112.42)
        }
    }

    private fun setupBottomSheet() {
        bottomView = findViewById(R.id.bottom_sheet)
        sheetBehavior = BottomSheetBehavior.from<RelativeLayout>(bottom_sheet)

        sheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            @SuppressLint("SwitchIntDef")
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                    }
                    BottomSheetBehavior.STATE_EXPANDED ->
                        Log.d("Respon Bottom Sheat", "Expand Bottom Sheet")
                    BottomSheetBehavior.STATE_COLLAPSED ->
                        Log.d("Respon Bottom Sheat", "Close Bottom Sheet")
                    BottomSheetBehavior.STATE_DRAGGING -> {
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }
        })
    }

    private fun expandCloseSheet() {
        if (sheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            Log.d("Respon Sheat", "Close Sheet")
        } else {
            sheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            Log.d("Respon Sheat", "Expand Sheet")
        }
    }

    override fun onSuccess(body: ResponModel) {
        swipeRefrash.isRefreshing = false
        display(body)
    }

    private fun display(body: ResponModel) {
        val statusCuaca = body.weather[0].main
        val deskripsiCuaca = body.weather[0].description
        val tempratur = "" + (body.main.temp - 273.15).toInt() +" \u2103"
        val tempraturMin = "" + (body.main.temp_min - 273.15).toInt() +" \u2103"
        val tempraturMax = "" + (body.main.temp_max - 273.15).toInt() +" \u2103"
        val tekanan = "" + body.main.pressure.toInt()
        val kelembaban = "" + body.main.humidity.toInt() + "%"
        val kecepatanAngin = "" + body.wind.speed + "mph"
        if (body.wind.deg == null) body.wind.deg = 0f
        val arahAngin = "" + body.wind.deg.toInt() +"\u00B0"
        val lokasi = body.name

        Log.d("respons", "Info cuaca: \n" +
                "statusCuaca : $statusCuaca \n" +
                "deskripsiCuaca : $deskripsiCuaca \n" +
                "tempratur : $tempratur \n" +
                "tempraturMin : $tempraturMin \n" +
                "tempraturMax : $tempraturMax \n" +
                "tekanan : $tekanan \n" +
                "kelembaban : $kelembaban \n" +
                "kecepatanAngin : $kecepatanAngin \n" +
                "arahAngin : $arahAngin")

        // setValue
        tv_cuaca.text = Helper.translate(statusCuaca, image_cuaca)
        tv_tempratur.text = tempratur
        tv_tempraturMin.text = tempraturMin
        tv_tempraturMax.text = tempraturMax
        tv_tekanan.text = tekanan
        tv_kelembaban.text = kelembaban
        tv_arahAngin.text = arahAngin
        tv_kecepatanAngin.text = kecepatanAngin
        tv_lokasi.text = lokasi
    }

    override fun onError(msg: String) {
        swipeRefrash.isRefreshing = false
        Toasty.error(this, msg, Toast.LENGTH_SHORT, true).show()
    }

    private fun injectDependency() {
        val activityComponent = DaggerActivityComponent.builder()
            .activityModule(ActivityModule(this))
            .build()

        activityComponent.inject(this)
    }

    private fun getKota(list: ArrayList<Kota>) {
        Log.d("Test", "Size"+list.size)
        if (list.size != 0){
            img_nodata.visibility = View.GONE
            val layoutTerlaris = LinearLayoutManager(this)
            layoutTerlaris.orientation = LinearLayoutManager.VERTICAL
            rv_place.layoutManager = layoutTerlaris
            rv_place.adapter = AdapterKota(list,  object : AdapterKota.Listeners {
                override fun onClicked(a: Kota) {
                    presenter.getData("" +a.nama)
                    hidekeyboard()
                    expandCloseSheet()
                }
            })
        }
    }

    private fun insertHistori(data: Kota) {
        compositeDisposable.add(Observable.fromCallable { db.daoHistoryKota().insert(data) }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Log.d("Berhasil", " tidak")
            })
    }

    private fun hidekeyboard() {
        val ub = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = currentFocus
        if (view == null) {
            view = View(applicationContext)
        }
        ub.hideSoftInputFromWindow(view.windowToken, 0)
        bottom_sheet!!.visibility = View.GONE
    }
}

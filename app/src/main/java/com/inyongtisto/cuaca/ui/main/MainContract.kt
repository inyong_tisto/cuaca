package com.inyongtisto.cuaca.ui.main

import com.inyongtisto.cuaca.model.ResponModel
import com.inyongtisto.cuaca.ui.base.BaseContract

class MainContract {

    interface View : BaseContract.View {
        fun onSuccess(body: ResponModel)
        fun onError(msg: String)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun getData(nama: String)
        fun getCuacaByLoc(lat: String, log:String)
    }
}
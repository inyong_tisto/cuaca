package com.inyongtisto.cuaca.di

import com.inyongtisto.cuaca.di.ActivityModule
import com.inyongtisto.cuaca.ui.main.MainActivity
import dagger.Component

@Component(modules = arrayOf(ActivityModule::class))
interface ActivityComponent {

    fun inject(mainActivity: MainActivity)

}
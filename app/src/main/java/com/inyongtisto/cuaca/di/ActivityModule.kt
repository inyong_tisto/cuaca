package com.inyongtisto.cuaca.di

import android.app.Activity
import com.inyongtisto.cuaca.ui.main.MainContract
import com.inyongtisto.cuaca.ui.main.MainPresenter
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private var activity: Activity) {

    @Provides
    fun provideActivity(): Activity {
        return activity
    }

    @Provides
    fun providePresenter(): MainContract.Presenter {
        return MainPresenter()
    }

}